#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio1(9, 8); // CE, CSN
//RF24 radio2(7, 6); // CE, CSN
const byte address1[6] = "00001";
const byte address2[6] = "00002";

const uint64_t pipe1 = 0xF0F0F0F0AA;
const uint64_t pipe2 = 0xF0F0F0F066;


struct sensor
{
  int sensorNum;
  int data;
};

sensor sensordet1;

sensor sensordet2;


void setup() {
  Serial.begin(9600);
  radio1.begin();
  radio1.openReadingPipe(1, pipe1);
  radio1.openReadingPipe(2, pipe2);
  radio1.setPALevel(RF24_PA_MIN);
  radio1.startListening();

  // radio2.begin();
  // radio2.openReadingPipe(2, address1);
  // radio2.setPALevel(RF24_PA_MIN);
  // radio2.startListening();
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
}

void loop() {
  //int x = 1;
  if (radio1.available(pipe1)) {
    char text[32] = "";
    int test;
    //int t2;
    radio1.read(&sensordet1, sizeof(sensordet1));
    //determine_data();
    radio1.read(&sensordet2, sizeof(sensordet2));
    determine_data();
  }
}

  
  void determine_data(){
    if(sensordet1.sensorNum == 1){
        if (sensordet1.data <= 5){
        digitalWrite(2, HIGH);
      }
      else {
        digitalWrite(2, LOW);
      }
    }

    else if(sensordet1.sensorNum ==2){
        if (sensordet1.data <= 5){
        digitalWrite(3, HIGH);
      }
      else {
        digitalWrite(3, LOW);
      }
    } 


    // if (sensordet2.data != 0){
    //   if(sensordet2.sensorNum == 1){
    //     if (sensordet2.data <= 5){
    //     digitalWrite(2, HIGH);
    //   }
    //   else {
    //     digitalWrite(2, LOW);
    //   }
    // }

    
  }