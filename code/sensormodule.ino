#define echoPin 5
#define echoPin2 4
#define echoPin3 3
#define pingPin 7
#define radioCE 9
#define radioCSN 8

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(radioCE, radioCSN); // CE, CSN

//const byte address[6] = "00001";

const uint64_t pipe1 = 0xF0F0F0F0AA;

struct sensor
{
  int sensorNum;
  int data;
};
sensor s1;



void setup() {

  Serial.begin(9600);
  //s1.begin();
  pinMode(pingPin, OUTPUT);
  pinMode(echoPin, INPUT);
  s1.sensorNum = 1;
  radio.begin();
  radio.stopListening();
  radio.openWritingPipe(pipe1);
  radio.setPALevel(RF24_PA_MIN);
  
}

void loop() {
  
  int distance = ping();
  Serial.println(distance);
  //radio.openWritingPipe(address);
  s1.data = distance;
  radio.write(&s1, sizeof(s1));
  delay(50);
}

int ping() {
  long duration;
  long duration2;
  long duration3;

  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(pingPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  delay(50);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(pingPin, LOW);
  duration2 = pulseIn(echoPin2, HIGH);
  delay(50);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(pingPin, LOW);
  duration3 = pulseIn(echoPin3, HIGH);
  int dist1,dist2,dist3;
  dist1 = msToCm(duration);
  dist2 = msToCm(duration2);
  dist3 = msToCm(duration3);
  // Serial.println(dist1);
  // Serial.println(dist2);
  // Serial.println(dist3);
  if (dist1 <= dist2 && dist1 <= dist3){
    return dist1;
  }
  else if (dist2 <= dist1 && dist2 <= dist3){
    return dist2;
  }
  return dist3;
}

long msToCm(long microseconds) {
  return microseconds / 29 / 2;
}