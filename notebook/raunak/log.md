# March 8, 2024

I began work on gathering resources for the sensor modules on our car and started planning out where on the cars I was going to place them. A lot of different preliminary design drafts have been made and I am still considering which one to use, as there are pros and cons to each one.
![Alt text](<Screenshot 2024-05-02 at 6.26.12 PM.png>)
![Alt text](<Screenshot 2024-05-02 at 6.27.06 PM.png>)
![Alt text](<Screenshot 2024-05-02 at 6.27.37 PM.png>)

The issue with the first two images I'm having right now is that there is a lot of dead space in front of the detection zone where obstacles would not be detected, and this may be against what we want to eliminate, even though both of them can cover a wider range, so I may opt to go for the third image, where all the dead zone is limited but we reduce the visibility to right in front of the car.

---

# March 25, 2024

We had our TA meeting and realized we were a little behind on the project, so I started assisting with PCB design help and worked on a starting point for the CAD modeling of our car. Because I have never experiemented with this software before or 3D printed parts, this will be a learning curve for me as I will definetely need lots of rough drafts before I can finalize a design. Here's what I have for the current center module with sizing constraints, a basic cubic 3D structure with space to fit our PCB, speaker, 9V battery, and 4 LEDs:
![Alt text](IMG_5324.JPG)

---

# March 31, 2024

Began actual work on modeling the previous design onto CAD and trying to get a prototype out by the end of the week for our casing so that we can start to experiement with different types of ideas. Right now I was able to import the previous design into CAD and here's what I achieved:
![Alt text](<Screenshot 2024-05-02 at 6.36.44 PM.png>)

I am going to 3D PLA print this tonight and get a rough idea on what needs to be changed to make it better

---

# April 1, 2024

I was able to get a 3D printed casing out for the center module and began work on the 4 corner sensor modules. The design for these had to include 3 30 degree angled sensors so that I could follow the design choice we decided to go with, space for a PCB, transciever, and 9V battery. This is the structure I worked on in the CAD software this week and will be printing later to test with actual part:
![Alt text](<Screenshot 2024-05-02 at 6.41.45 PM.png>)

---

# April 14, 2024

We realized that our PCB is not functioning and we wouldn't be able to order a new one by the time of mock demo, so we are now switching over to arudinos in each of the modules to communicate with the parts, changing the design schematics dramatically. The center module needs more space for a mini bread board so the recievers inside it can sit and be wired to the arduino, and the sensor modules need some height for wiring reasons and an additionally oversized arduino. Here are the new prototypes that I was able to get sketched out, including lids that let us close and access parts to the modules:
![Alt text](<Screenshot 2024-05-02 at 6.45.38 PM.png>)
![Alt text](<Screenshot 2024-05-02 at 6.46.23 PM.png>)

I was able to get these printed quicker and fit all the individual parts into the corresponding places, and we are all going to begin testing shortly on this.

---

# April 19, 2024

Now that our demo date is approaching, we are finalizing the product and testing it on an actual car:
![Alt text](<Screenshot 2024-05-02 at 6.47.42 PM.png>)

We were able to run some R&V's to verify the integrity of the design, and things are looking good for the project!

---

# April 25, 2024

We presented our final demo and are now going to begin work on the presentation slides and lab notebook to wrap up the project.


