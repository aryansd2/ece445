# March 1, 2024 
---
After our design review, we started looking into the parts we needed to order, mainly for the PCB as I was in charge of that. Using the mini-encabulator lab as a guide, I looked into the proper resistors, capacitors, and potentially oscillators that we would need - I wasn't worried about selecting the sensors, transceivers, or microcontroller as this was already determined. Started making a list of parts we would need.

---
# March 8, 2024
Parts list almost finalized and sensors/transceivers were ordered through third party to get them a bit earlier/they were not available through the parts available through the provided resources, started to think about PCB design, how to structure our board, and if we really needed 2 different boards.

---
# March 20, 2024
After our weekly TA meeting, we made sure our parts that could be ordered through the shop were finalized and requested them later this week, PCB schematic starting to come together - learning KiCad as I go due to 0 prior experience. 

---
# March 24, 2024
Schematic complete, starting to work on routing - schematic is pictured below:
![Alt text](<unnamed.png>)
This was fairly easy to do, routing seems to be more challenging as I need to figure out how to lay out the components on the board. At this stage, we feel that size constraints were the least of our worries, so I opted for a larger board size and started routing.

---
# March 27, 2024
PCB routing turned out to be more time consuming than intended due to misunderstanding of how ground nets and vias worked, missed this week's deadline for the PCB orders through our TA. Routing was mostly complete and PCB is ready to be ordered. Pictured below is the final board we decided to use:
![Alt text](<pcbrouting.png>)
Collectively made the decision to utilize the same board for both our sensor module and central module, as this would save on time (we are slightly behind schedule at this stage). The essential components needed were present, which were the ATMegas and the transceiver module, the only difference would be that the sensor module would utilize the through holes placed for the Ultrasonic sensors, while the central module would not. 

---
# April 1, 2024
---
PCB Order was placed through TA, all parts have now been delivered and we are waiting on our PCBs. Meanwhile, testing is being done through the transceivers and ultrasonic sensors we ordered early by using arduinos and a breadboard. The main issue was figuring out the multiple transmitters to one receiver code with the nrf24l01 transceivers we had. Ultrasonic sensors seemed to be working without issue.

---
# April 4, 2024
---
We have realized that if we simply wait for the PCB to come through the order placed by our TA, we might be waiting for a lot longer than allows us time to do adequate testing, thus I ordered third party through PCBway to get our boards a bit faster.

---
# April 9, 2024
---
Boards from third party order were delivered, and we were right to assume that the boards ordered through our TA wouldn't arrive by this time. 

---
# April 10, 2024
---
Went to the lab to use the reflow oven and begin soldering components onto the PCB. Also started testing our PCB procedurally after all components were soldered, however we ran into some issues here:
- First, verified continuity with multimeter and this was successful
- Connected our PCB to the 9 volt battery and verified that our linear regulators were indeed getting the power down to 5V for the ATMega and Ultrasonic sensors, and 3.3 V for the transceiver. This was also successful.
- Next, we tested that we could program our ATMega. We uploaded a simple flashing LED program to it, used the USBasp to program it, and hooked up an LED to the debug pins we had on our board. This was also ssuccessful.
- The issues started here, as we started testing our transceiver on the PCB. When first soldered on and the code from our breadboard testing was uploaded, it seemed to not be transmiting any information. We printed various messages to our console output in the arduino IDE, and noted that it wasn't sending any data.
-  Transceiver was receiving power, but not sending data, we figured it might have been faulty. We used a heat gun to remove the component from the PCB and used wires to connect it to our breadboard setup, and it was working. This proved we had an isseu with the PCB.

This day in the lab was relatively long with all the testing, decided to test again another day to narrow down the issue and see if we could solve it before it was too late to order another round of PCBs

---
# April 12, 2024
---
I went back to lab to continue testing, though we are getting worried about the PCB's success as the mock demo and demo dates are fast appproaching. As such we have moved to try and use arduinos from the lab and get our design fully working regardless of the PCB. 
Testing today went as follows:
- Reverified that power was being delivered to the transceiver - it was.
- Started using debug pins to make sure all could light an LED to make sure it wasn't an issue with the programming of the microcontroller.
- Used ribbon cables to hook up a spare ultrasonic sensor to the debug pins and an LED to indicate a threshold distance. Somehow, this code worked on our PCB and the ultrasonic sensor worked, the only problem was our transceiver. 
- After switching entire boards, reprogramming and retesting both the ultrasonic sensors and the transceiver with no success, I believed that it must have been a problem with our linear regulator for 3.3 Volts as that was the only difference between the power being delivered to the ultrasonic sensors and ATMega versus the transceiver. 
- Decided that the boards were not going to end up working as intended, and kept the flashing LED/working ultrasonic sensor as proof that our microcontroller on PCB was able to be programmed successfully along with some of the peripherals. 

After today, I decided to push our group to focus on getting our project to be functional fully on the arduino and mini breadboards, as well as assisting with the 3D modeling of our actual sensor modules.
# April 19, 2024
---
Mock demo complete, demonstrated project code working on breadboard and arduino as we finalized our 3d designs and started to use the 3D printers to build them. PCB still had no success after further testing, so we fully committed to the arduinos and modeled our casing to accomodate the extra space it would take up.
# April 25, 2024
---
Final demo complete and project mostly working on arduino.

---

