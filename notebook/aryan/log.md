# March 8, 2024

## Parts research
I was doing research on how to have effective communication between each of our sensor  modules, as we decided to have 4 seaprate ones that needed to all simultaneously transmit and a receiver that is able to distinguish between each of the different transmitters, and the original plan was to use 433 MHz transmitters but I don't think that they would work without interference, so I'm looking at the NRF24L01 module right now.
https://www.sparkfun.com/datasheets/Components/SMD/nRF24L01Pluss_Preliminary_Product_Specification_v1_0.pdf

---

# March 25, 2024

Had our weekly TA meeting, team should start moving into finalizing parts olist, we stayed with the ultrasonic sensor so I started using an old one I had to test the accuracy and FOV of it, and started writing the code for our whole module, but the testing code I'm using is below. All components our ordered, using a 9 volt battery seemed like the best decision for our use cases as well

```
#define echoPin 5
#define pingPin 3

void setup() {
// put your setup code here, to run once:

Serial.begin(9600);
    pinMode(pingPin, OUTPUT);
    pinMode(echoPin, INPUT);
}

void loop() {
// put your main code here, to run repeatedly:
int distance = msToCm( ping() );
Serial.println(distance);
}

// Helper function to manage our ultrasonic sensor.
long ping() {
long duration;
digitalWrite(pingPin, LOW);
delayMicroseconds(2);
digitalWrite(pingPin, HIGH);
delayMicroseconds(10);
digitalWrite(pingPin, LOW);
duration = pulseIn(echoPin, HIGH);

return duration;
}

long msToCm(long microseconds) {
return microseconds / 29 / 2;
}
```
Also did some physical testing below 



![Alt text](testing.png)
---

# March 31, 2024

Testing for ultrasonic sensor and the transciever works
https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf
FOV and accuracy is good enough for our use case.
Transceiver test code is also attached below, and I tested it through obstacles and on an actual car and it all worked fine, with really low latency, and no interference.
Transmitter
```
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7, 8); // CE, CSN

const byte address[6] = "00001";

void setup() {
  radio.begin();
  radio.openWritingPipe(address);
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();
}

void loop() {
  const char text[] = "Hello World";
  radio.write(&text, sizeof(text));
  delay(1000);
}
```

Receiver
```
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7, 8); // CE, CSN

const byte address[6] = "00001";

void setup() {
  Serial.begin(9600);
  radio.begin();
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();
}

void loop() {
  if (radio.available()) {
    char text[32] = "";
    radio.read(&text, sizeof(text));
    Serial.println(text);
  }
}
```
# April 1, 2024

I helped with the schematic design, and we were able to place our final PCB order this week, the multi sensor code is done and tested, and should theoretically be the exact same other than pin configuration on the arduino I am using for testing as it is on the actual PCB.
```
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio1(9, 8); // CE, CSN
//RF24 radio2(7, 6); // CE, CSN
const byte address1[6] = "00001";
const byte address2[6] = "00002";

const uint64_t pipe1 = 0xF0F0F0F0AA;
const uint64_t pipe2 = 0xF0F0F0F066;


struct sensor
{
  int sensorNum;
  int data;
};

sensor sensordet1;

sensor sensordet2;


void setup() {
  Serial.begin(9600);
  radio1.begin();
  radio1.openReadingPipe(1, pipe1);
  radio1.openReadingPipe(2, pipe2);
  radio1.setPALevel(RF24_PA_MIN);
  radio1.startListening();

  // radio2.begin();
  // radio2.openReadingPipe(2, address1);
  // radio2.setPALevel(RF24_PA_MIN);
  // radio2.startListening();
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
}

void loop() {
  //int x = 1;
  if (radio1.available(pipe1)) {
    char text[32] = "";
    int test;
    //int t2;
    radio1.read(&sensordet1, sizeof(sensordet1));
    //determine_data();
    radio1.read(&sensordet2, sizeof(sensordet2));
    determine_data();
  }
}

  
  void determine_data(){
    if(sensordet1.sensorNum == 1){
        if (sensordet1.data <= 5){
        digitalWrite(2, HIGH);
      }
      else {
        digitalWrite(2, LOW);
      }
    }

    else if(sensordet1.sensorNum ==2){
        if (sensordet1.data <= 5){
        digitalWrite(3, HIGH);
      }
      else {
        digitalWrite(3, LOW);
      }
    } 

  }
```

# April 7, 2024

Working on soldering PCB and gathering all components from Amazon, ECEB Supply Center and electronics shop, did some more testing with the angles for the ultrasonic sensors, which is below. Additionally, we were able to calculate the angle offsets for each sensor on the final mounted part, whihc worked with the FOV testing data that we had.

| Angle (deg) | Distance Measured (cm) |
| ----------- | ---------------------- |
| \-15        | 28                     |
| \-10        | 29                     |
| \-5         | 29                     |
| 0           | 30                     |
| 5           | 29                     |
| 10          | 29                     |
| 15          | 28                     |
| Average     | 28.85714286            |

# April 14, 2024

Started working on final presentation, finished soldering all components onto PCB we are currently in the process of testing the individual components, as we seem to have ordered some faulty sensors and transceivers. Whole project worked fairly well on arduino, and it seems like multiple NRF24L01 chips may be needed on the central receiver module, but we are unsure. Testing with all 3 sensor atached also this time with output data.
| Actual Distance (cm) | Measured Distance (cm) | Measured - Actual |
| -------------------- | ---------------------- | ----------------- |
| 5                    | 6                      | \-1               |
| 10                   | 12                     | \-2               |
| 15                   | 17                     | \-2               |
| 20                   | 21                     | \-1               |
| 25                   | 26                     | \-1               |
| 30                   | 32                     | \-2               |
| 35                   | 36                     | \-1               |
| 40                   | 41                     | \-1               |
| 45                   | 47                     | \-2               |
| 50                   | 54                     | \-4               |
| 55                   | 58                     | \-3               |
| 60                   | 65                     | \-5               |
|                      |                        | \-2.083333333     |

Also decided on what the speaker sound we wanted to hear is, so test speaker code withi equation based on distance is below.
```
int start = 0;
int interval = 99999;
bool speakeron = False;
if (distance<threshold){
    interval = 200 - 3 * distance;
    if(millis()-start >= threshold){
        if (speakeron){
            digitalWrite(speaker, LOW);
        } else{
            digitalWrite(speaker, HIGH);
        }
        speakeron = !speakeron;
        start= millis()
    }

} else{
    start = millis();
    digitalWrite(speaker, LOW);
    speakeron = False;
}
```
---

# April 21, 2024

Final Demo was done, we used Arduinos, and we had a full working design, powered by 9 volt batteries, I am now starting to work on the final presentation. We are also doing our final demo, which I worked on converting overything to Arduino for, so all of the sensors and transceivers would work properly and transmit.

---



# April 25, 2024

Final Demo was done, we used Arduinos, and we had a full working design, powered by 9 volt batteries, I am now starting to work on the final presentation. It is going well, and we were able to test on an actual vehicle and everything, which is attached below.


![Alt text](testing2.jpg)

---


